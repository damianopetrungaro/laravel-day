<?php

namespace App\Http\Controllers;

use App\Model\UserEloquent;
use Illuminate\Http\Request;

class GetUser1 extends Controller
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $name = $request->get('name', 'Damiano');

        return UserEloquent::where('name', $name)->first() ?: "No $name were found";
    }
}
