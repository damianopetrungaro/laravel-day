<?php

namespace App\Http\Controllers;

use App\Model\UserEloquent;
use Illuminate\Http\Request;

class GetUser2 extends Controller
{
    /**
     * @var UserEloquent
     */
    private $userModel;

    /**
     * GetUser2 constructor.
     *
     * @param UserEloquent $userModel
     */
    public function __construct(UserEloquent $userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $name = $request->get('name', 'Damiano');

        return $this->userModel->where('name', $name)->first() ?: "No $name were found";
    }
}
