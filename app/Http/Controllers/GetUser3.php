<?php

namespace App\Http\Controllers;

use App\Repository\BadUserRepository;
use Illuminate\Http\Request;

class GetUser3 extends Controller
{
    /**
     * @var BadUserRepository
     */
    private $userRepository;

    /**
     * GetUser3 constructor.
     *
     * @param BadUserRepository $userRepository
     */
    public function __construct(BadUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $name = $request->get('name', 'Damiano');

        if ($user = $this->userRepository->findByName($name)) {
            // We do not want this
            // $user->delete();
        }

        return $user ?: "No $name were found";
    }
}
