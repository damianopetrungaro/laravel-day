<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use Illuminate\Http\Request;

class GetUser4 extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * GetUser3 constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $name = $request->get('name', 'Damiano');

        $user = $this->userRepository->findByName($name);
        // We now are safe from this
        // $user->delete();

        return $user ?: "No $name were found";
    }
}
