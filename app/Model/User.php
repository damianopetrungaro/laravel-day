<?php

namespace App\Model;

interface User
{
    /**
     * @return int
     */
    public function id(): int;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return string
     */
    public function email(): string;
}
