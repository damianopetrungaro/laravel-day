<?php

namespace App\Repository;

use App\Model\UserEloquent;

class BadUserRepository
{
    /**
     * @var UserEloquent
     */
    private $userModel;

    /**
     * UserEloquentRepository constructor.
     *
     * @param UserEloquent $userModel
     */
    public function __construct(UserEloquent $userModel)
    {
        $this->userModel = $userModel;
    }

    public function findByName(string $name):? UserEloquent
    {
        return $this->userModel->where('name', $name)->first();
    }
}