<?php

namespace App\Repository;

use App\Model\User;
use App\Model\UserEloquent;

class UserEloquentRepository implements UserRepository
{
    /**
     * @var UserEloquent
     */
    private $userModel;

    /**
     * UserEloquentRepository constructor.
     *
     * @param UserEloquent $userModel
     */
    public function __construct(UserEloquent $userModel)
    {
        $this->userModel = $userModel;
    }

    public function findByName(string $name):? User
    {
        return $this->userModel->where('name', $name)->first();
    }

    public function add(User $user): void
    {
        //
    }
}