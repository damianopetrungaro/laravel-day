<?php

namespace App\Repository;

use App\Model\User;

interface UserRepository
{
    public function findByName(string $name):? User;

    public function add(User $user): void;
}