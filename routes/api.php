<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users1', 'GetUser1');
Route::get('/users2', 'GetUser2');
Route::get('/users3', 'GetUser3');
Route::get('/users4', 'GetUser4');
